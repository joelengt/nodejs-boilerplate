import { noop } from './noop'

test('noop value', () => {
  expect(noop(3)).toBe(3)
})
