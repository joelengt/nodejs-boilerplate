
import { sample } from './sample'

function sum (a, b) {
  return a + b
}

function fetchData (d) {
  return d
}

test('sample val', () => {
  let val = sample(1, 3)
  expect(val).toBe(4)
})

describe('matching cities to foods', () => {
  test('value', () => {
    expect(4).toBe(4)
  })
  test('adds 1 + 2 to equal 3', () => {
    expect(sum(1, 2)).toBe(3)
  })
})

test('the data is peanut butter', async () => {
    expect.assertions(1)
    const data = await fetchData()
    expect(data).toBe('peanut butter')
})

test('the fetch fails with an error', async () => {
  expect.assertions(1)
  try {
    await fetchData()
  } catch (e) {
    expect(e).toMatch('error')
  }
})

test('the data is peanut butter', async () => {
  expect.assertions(1)
  await expect(fetchData()).resolves.toBe('peanut butter')
})

test('the fetch fails with an error', async () => {
  expect.assertions(1)
  await expect(fetchData()).rejects.toMatch('error')
})

function forEach(items, callback) {
  for (let index = 0; index < items.length; index++) {
    callback(items[index]);
  }
}

const mockCallback = jest.fn();

test('a pretty sample', () => {

  mockCallback
    .mockReturnValueOnce(10)
    .mockReturnValueOnce('x')
    .mockReturnValue(true);

  mockCallback()

  forEach([1, 2], mockCallback)
  expect(mockCallback.mock.calls).toBe(2);
  expect(mockCallback.mock.calls).toBe();
})

const mockImplementation = jest.fn(cb => (null, true))
test('mock implementation', {
  mockImplementation((err, val) => console.log(val))
})


const myMockFn = jest
  .fn(() => 'default')
  .mockImplementationOnce(cb => cb(null, true))
  .mockImplementationOnce(cb => cb(null, false));

myMockFn((err, val) => console.log(val));
// > true

myMockFn((err, val) => console.log(val));


// ---------------------------------------

const sampleMock = jest.fn()

test('sample mocking function', () => {
  forEach([1, 2], sampleMock)
  expect(sampleMock.mock.calls).toBe(2)
})


// ---------------------------------------

const sampleMockReturnDefault = jest.fn()

test('sample mocking with default return', () => {
  sampleMockReturnDefault
    .mockReturnValueOnce(false)
    .mockReturnValue(true)

  sampleMockReturnDefault()
  
})


// ---------------------------------------

test('mock implementation', () => {
  const mockingImplementation = jest
    .fn(cb => 'default')
    .mockImplementationOnce(cb => (null, true))
    .mockImplementationOnce(cb => (null, false))

  mockingImplementation((err, value) => console.log(value))
  mockingImplementation((err, value) => console.log(value))
  
})
